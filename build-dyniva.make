api = 2
core = 8.x
includes[] = drupal-org-core.make
projects[dyniva][type] = profile
projects[dyniva][download][type] = git
projects[dyniva][download][url] = "https://github.com/terryzwt/dyniva"
projects[dyniva][download][branch] = master
